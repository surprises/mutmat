import locale
import os
from pathlib import Path

DATA_ROOT = Path(__file__).parent.parent / "db"
LOG_ROOT = Path("/tmp")
SECRET = "sikretfordevonly"
JWT_ALGORITHM = "HS256"
SEND_EMAILS = False
SMTP_HOST = "mail.gandi.net"
SMTP_PASSWORD = ""
SMTP_LOGIN = ""
FROM_EMAIL = "me@mutmat"
STAFF = []
LOCALE = "fr_FR.UTF-8"
SITE_NAME = "La Mutmat"
CONTACT_EMAIL = "mutmat@riseup.net"
SITE_DESCRIPTION = "La mutmat est une mutuelle de matériel d'évennementiel, basée sur Rennes."
EMAIL_SIGNATURE = "La gentille robote de la mutmat"


def init():
    for key, value in globals().items():
        if key.isupper():
            env_key = "MUTMAT_" + key
            typ = type(value)
            if typ == list:
                typ = lambda x: x.split()
            if env_key in os.environ:
                globals()[key] = typ(os.environ[env_key])
    locale.setlocale(locale.LC_ALL, LOCALE)


init()
